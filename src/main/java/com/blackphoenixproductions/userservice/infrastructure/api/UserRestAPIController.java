package com.blackphoenixproductions.userservice.infrastructure.api;

import com.blackphoenixproductions.commons.enums.Claims;
import com.blackphoenixproductions.commons.enums.Roles;
import com.blackphoenixproductions.userservice.domain.IUserDAO;
import com.blackphoenixproductions.userservice.domain.IUserService;
import com.blackphoenixproductions.userservice.domain.model.User;
import com.nimbusds.jose.shaded.gson.internal.LinkedTreeMap;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("/users")
@Tag(name = "User", description = "endpoints riguardanti gli utenti.")
public class UserRestAPIController {

    private static final Logger logger = LoggerFactory.getLogger(UserRestAPIController.class);

    private final IUserDAO userDAO;
    private final IUserService userService;

    @Autowired
    public UserRestAPIController(IUserDAO userDAO, IUserService userService) {
        this.userDAO = userDAO;
        this.userService = userService;
    }


    @Operation(summary = "Restituisce il numero totale degli utenti.")
    @GetMapping (value = "/getTotalUsers")
    public ResponseEntity<Long> getTotalUsers (HttpServletRequest req) {
        Long totalUsers = userDAO.getTotalUsers();
        return new ResponseEntity<Long>(totalUsers, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @Operation(summary = "Restituisce il jwt dell'utente loggato")
    @GetMapping(value = "/getJwt")
    public ResponseEntity<Jwt> getJwt (HttpServletRequest req, Principal principal, @AuthenticationPrincipal Jwt jwt){
        return new ResponseEntity<>(jwt, HttpStatus.OK);
    }


    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Registra l'utente sull'applicativo dopo la registrazione dell'utente con Keycloak.")
    @GetMapping (value = "/registerUserFromToken")
    public ResponseEntity<User> registerUserFromToken (HttpServletRequest req, @AuthenticationPrincipal Jwt jwt){
        logger.info("Start registerUser");
        String username = (String) jwt.getClaims().get(Claims.USERNAME.getValue());
        String email = (String) jwt.getClaims().get(Claims.EMAIL.getValue());
        LinkedTreeMap<String, ArrayList> realmAccess = (LinkedTreeMap<String, ArrayList>) jwt.getClaims().get(Claims.REALM.getValue());
        ArrayList<String> roles = realmAccess.get(Claims.ROLES.getValue());
        String role = roles.stream().filter(r -> Roles.getAllRoles().contains(r)).findAny().orElse(null);
        User registeredUser = userService.registerUser(username, email, role);
        logger.info("End registerUser");
        return new ResponseEntity<User>(registeredUser, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Registra l'utente.")
    @GetMapping (value = "/registerUser")
    public ResponseEntity<User> registerUser (HttpServletRequest req,
                                              @Parameter(description = "l'username dell'utente") @RequestParam String username,
                                              @Parameter(description = "l'email dell'utente") @RequestParam String email,
                                              @Parameter(description = "Il ruolo dell'utente") @RequestParam String role){
        logger.info("Start registerUser");
        User registeredUser = userService.registerUser(username, email, role);
        logger.info("End registerUser");
        return new ResponseEntity<User>(registeredUser, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Cerca l'utente loggato nell'applicativo.")
    @GetMapping (value = "/retriveUser")
    public ResponseEntity<User> retriveUser (HttpServletRequest req,  @AuthenticationPrincipal Jwt jwt){
        logger.info("Start retriveUser");
        String email = (String) jwt.getClaims().get(Claims.EMAIL.getValue());
        User findedUser = userDAO.getUserFromEmail(email);
        logger.info("End retriveUser");
        return new ResponseEntity<User>(findedUser, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(schema = @Schema(hidden = true))),
            @ApiResponse(responseCode = "404", description = "Not Found.", content = @Content(schema = @Schema(hidden = true))),
    })
    @Operation(summary = "Cambia l'username dell'utente loggato.")
    @PostMapping (value = "/changeUserUsername")
    public ResponseEntity<User> changeUserUsername (HttpServletRequest req,
                                                                 @AuthenticationPrincipal Jwt jwt,
                                                                 @Parameter(description = "Il nuovo username") @RequestParam String newUsername){
        logger.info("Start changeUserUsername");
        String email = (String) jwt.getClaims().get(Claims.EMAIL.getValue());
        User user = userService.changeUsername(jwt.getSubject(), email, newUsername);
        logger.info("End changeUserUsername");
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }


}