package com.blackphoenixproductions.userservice.infrastructure.api;

import com.blackphoenixproductions.commons.dto.ErrorResponse;
import com.blackphoenixproductions.commons.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class GlobalExceptionHandlerController {

  public static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandlerController.class);

  @ExceptionHandler(CustomException.class)
  public ResponseEntity<ErrorResponse> handleCustomException(CustomException ex) {
    logger.error(ex.getMessage(), ex);
    return new ResponseEntity<ErrorResponse>(new ErrorResponse(ex.getMessage(), ex.getHttpStatus()), ex.getHttpStatus());
  }

  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponse> handleException(Exception ex) {
    logger.error(ex.getMessage(), ex);
    return new ResponseEntity<ErrorResponse>(new ErrorResponse(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
