package com.blackphoenixproductions.userservice.infrastructure.iam;

import com.blackphoenixproductions.commons.exception.CustomException;
import com.blackphoenixproductions.userservice.domain.IKeycloakProxy;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;


@Component("keycloakProxyBuilder")
public class KeycloakProxy implements IKeycloakProxy {
    private String KEYCLOAK_SERVER_URL;
    private String KEYCLOAK_REALM;
    private String KEYCLOAK_RESOURCE;
    private String CLIENT_SECRET;


    @Autowired
    public KeycloakProxy(@Value("${keycloak.auth-server-url}") String KEYCLOAK_SERVER_URL,
                         @Value("${keycloak.realm}") String KEYCLOAK_REALM,
                         @Value("${keycloak.resource}") String KEYCLOAK_RESOURCE,
                         @Value("${keycloak.clientsecret}") String CLIENT_SECRET)  {
        this.KEYCLOAK_SERVER_URL = KEYCLOAK_SERVER_URL;
        this.KEYCLOAK_REALM = KEYCLOAK_REALM;
        this.KEYCLOAK_RESOURCE = KEYCLOAK_RESOURCE;
        this.CLIENT_SECRET = CLIENT_SECRET;
    }

    @Override
    public void changeUserUsername(String subject, String newUsername) {
        try {
            Keycloak kc = KeycloakBuilder.builder()
                    .serverUrl(KEYCLOAK_SERVER_URL)
                    .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                    .realm(KEYCLOAK_REALM)
                    .clientId(KEYCLOAK_RESOURCE)
                    .clientSecret(CLIENT_SECRET)
                    .build();
            UserRepresentation keycloakUser = kc.realm(KEYCLOAK_REALM).users().get(subject).toRepresentation();
            keycloakUser.setUsername(newUsername);
            kc.realm(KEYCLOAK_REALM).users().get(subject).update(keycloakUser);
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
