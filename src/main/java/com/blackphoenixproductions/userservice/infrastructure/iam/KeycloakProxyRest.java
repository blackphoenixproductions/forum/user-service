package com.blackphoenixproductions.userservice.infrastructure.iam;

import com.blackphoenixproductions.commons.exception.CustomException;
import com.blackphoenixproductions.userservice.domain.IKeycloakProxy;
import org.keycloak.OAuth2Constants;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;


@Component("keycloakProxyRest")
public class KeycloakProxyRest implements IKeycloakProxy {
    private String KEYCLOAK_SERVER_URL;
    private String KEYCLOAK_REALM;
    private String KEYCLOAK_RESOURCE;
    private String CLIENT_SECRET;
    private final RestTemplate restTemplate;


    @Autowired
    public KeycloakProxyRest(@Value("${keycloak.auth-server-url}") String KEYCLOAK_SERVER_URL,
                             @Value("${keycloak.realm}") String KEYCLOAK_REALM,
                             @Value("${keycloak.resource}") String KEYCLOAK_RESOURCE,
                             @Value("${keycloak.clientsecret}") String CLIENT_SECRET, RestTemplate restTemplate)  {
        this.KEYCLOAK_SERVER_URL = KEYCLOAK_SERVER_URL;
        this.KEYCLOAK_REALM = KEYCLOAK_REALM;
        this.KEYCLOAK_RESOURCE = KEYCLOAK_RESOURCE;
        this.CLIENT_SECRET = CLIENT_SECRET;
        this.restTemplate = restTemplate;
    }

    @Override
    public void changeUserUsername(String subject, String newUsername) {
        UriComponents builder = UriComponentsBuilder
                .fromHttpUrl(String.format("%s/realms/%s/protocol/openid-connect/token",
                        KEYCLOAK_SERVER_URL, KEYCLOAK_REALM))
                .build();

        MultiValueMap<String, String> body = getRequestBody();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);

        AccessTokenResponse accessTokenResponse = restTemplate.postForObject(builder.toUri(),
                request, AccessTokenResponse.class);

        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBearerAuth(accessTokenResponse.getToken());
        request = new HttpEntity<>(headers);

        ResponseEntity<UserRepresentation> resp = null;
        try {
            resp = restTemplate.exchange(String.format("%s/admin/realms/%s/users/%s",
                    KEYCLOAK_SERVER_URL, KEYCLOAK_REALM, subject),
                    HttpMethod.GET, request, UserRepresentation.class);
            if(!resp.getStatusCode().is2xxSuccessful()) {
                throw new Exception("Errore chiamata keycloak");
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        UserRepresentation user = resp.getBody();
        user.setUsername(newUsername);

        HttpEntity<Object> objRequest = new HttpEntity<>(user, headers);
        ResponseEntity<Void> response = null;
        try {
            response = restTemplate.exchange(String.format("%s/admin/realms/%s/users/%s",
                            KEYCLOAK_SERVER_URL, KEYCLOAK_REALM, subject),
                    HttpMethod.PUT, objRequest, Void.class);
            if(!response.getStatusCode().is2xxSuccessful()) {
                throw new Exception("Errore chiamata aggiornamento utente keycloak");
            }
        } catch (Exception e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    private MultiValueMap<String, String> getRequestBody() {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("grant_type", OAuth2Constants.CLIENT_CREDENTIALS);
        body.add("client_id", KEYCLOAK_RESOURCE);
        body.add("client_secret", CLIENT_SECRET);
        body.add("realm", KEYCLOAK_REALM);
        return body;
    }
}
