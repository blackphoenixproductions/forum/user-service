package com.blackphoenixproductions.userservice.infrastructure.repository;

import com.blackphoenixproductions.userservice.infrastructure.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserEntityRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByEmail(String email);
}
