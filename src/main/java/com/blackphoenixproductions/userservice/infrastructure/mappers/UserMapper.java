package com.blackphoenixproductions.userservice.infrastructure.mappers;

import com.blackphoenixproductions.userservice.domain.model.User;
import com.blackphoenixproductions.userservice.infrastructure.entity.UserEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserMapper {
    UserEntity userToUserEntity(User user);
    User userEntityToUser(UserEntity user);
}
