package com.blackphoenixproductions.userservice.infrastructure.dao;

import com.blackphoenixproductions.commons.exception.CustomException;
import com.blackphoenixproductions.userservice.domain.IUserDAO;
import com.blackphoenixproductions.userservice.domain.model.User;
import com.blackphoenixproductions.userservice.infrastructure.entity.UserEntity;
import com.blackphoenixproductions.userservice.infrastructure.mappers.UserMapper;
import com.blackphoenixproductions.userservice.infrastructure.repository.UserEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO implements IUserDAO {

    private final UserEntityRepository userEntityRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserDAO(UserEntityRepository userEntityRepository, UserMapper userMapper) {
        this.userEntityRepository = userEntityRepository;
        this.userMapper = userMapper;
    }

    @Override
    public User saveUser(User user) {
        UserEntity userEntity = userMapper.userToUserEntity(user);
        UserEntity savedUser = userEntityRepository.saveAndFlush(userEntity);
        return userMapper.userEntityToUser(savedUser);
    }

    @Override
    public User getUserFromEmail(String email) {
        UserEntity findedUser = userEntityRepository.findByEmail(email);
        if(findedUser == null){
            throw  new CustomException("User not found", HttpStatus.NOT_FOUND);
        }
        return userMapper.userEntityToUser(findedUser);
    }

    @Override
    public Long getTotalUsers() {
        return userEntityRepository.count();
    }

    @Override
    public User changeUsername(String email, String newUsername) {
        UserEntity findedUser = userEntityRepository.findByEmail(email);
        if(findedUser == null){
            throw  new CustomException("User not found", HttpStatus.NOT_FOUND);
        }
        findedUser.setUsername(newUsername);
        UserEntity savedUser = userEntityRepository.saveAndFlush(findedUser);
        return userMapper.userEntityToUser(savedUser);
    }

    @Override
    public User saveUser(String username, String email, String role) {
        UserEntity findedUser = userEntityRepository.findByEmail(email);
        if(findedUser != null){
            throw  new CustomException("User already exist", HttpStatus.CONFLICT);
        }
        UserEntity newUser = new UserEntity(username, email, role);
        UserEntity savedUser = userEntityRepository.saveAndFlush(newUser);
        return userMapper.userEntityToUser(savedUser);
    }
}
