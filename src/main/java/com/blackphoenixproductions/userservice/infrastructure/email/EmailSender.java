package com.blackphoenixproductions.userservice.infrastructure.email;

import com.blackphoenixproductions.userservice.domain.IEmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import jakarta.mail.internet.MimeMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;


@Component
public class EmailSender implements IEmailSender {

    private final JavaMailSender javaMailSender;
    private static final Logger logger = LoggerFactory.getLogger(EmailSender.class);


    @Autowired
    public EmailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    private void send(String to, String subject, String body)  {
        try {
            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper;
            helper = new MimeMessageHelper(message, true);
            helper.setSubject(subject);
            helper.setTo(to);
            helper.setText("<img src='cid:logo'>" + body, true);
            ClassPathResource res = new ClassPathResource("/static/blackphoenix@2x.png");
            helper.addInline("logo", res);
            javaMailSender.send(message);
        } catch (Exception e){
            logger.error("Errore durante l'invio dell'email.");
        }
    }


    @Override
    public void sendRegistrationEmail(String username, String email) {
        StringBuilder message = new StringBuilder("<h1>Benvenuto nel nostro forum " + username + "!</h1><br>");
        message.append("<h2>Ti ringraziamo per esserti registrato, ora puoi partecipare attivamente alla nostra community!</h2>");
        setEmailFooter(message);
        send(email, "Benvenuto nel nostro forum!", message.toString());
    }


    private void setEmailFooter(StringBuilder body){
        body.append("<br><br><br>&#169; All Rights Reserved 2021 by Black Phoenix Productions.");
    }

}
