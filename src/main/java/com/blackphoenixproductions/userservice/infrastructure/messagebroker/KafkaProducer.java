package com.blackphoenixproductions.userservice.infrastructure.messagebroker;

import com.blackphoenixproductions.commons.constants.Topics;
import com.blackphoenixproductions.userservice.domain.MessagePublisher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer implements MessagePublisher {

    private static final Logger logger = LoggerFactory.getLogger(KafkaProducer.class);

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public KafkaProducer(KafkaTemplate<String, String> kafkaTemplate, ObjectMapper objectMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
    }

    public void refreshMaterializedViews()
    {
        logger.info("Sending message to topic-service and post-service...");
        this.kafkaTemplate.send(Topics.REFRESH_MV_TOPICS, "refresh");
        this.kafkaTemplate.send(Topics.REFRESH_MV_POSTS, "refresh");
    }

}
