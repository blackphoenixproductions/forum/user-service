package com.blackphoenixproductions.userservice.domain;


public interface IKeycloakProxy {
    void changeUserUsername(String subject, String newUsername);
}
