package com.blackphoenixproductions.userservice.domain;

public interface IEmailSender {
    void sendRegistrationEmail(String username, String email);
}
