package com.blackphoenixproductions.userservice.domain;

import com.blackphoenixproductions.userservice.domain.model.User;

public interface IUserService {
    User registerUser(String username, String email, String role);
    User changeUsername(String subject, String email, String newUsername);
    User saveUser(String username, String email, String role);
}
