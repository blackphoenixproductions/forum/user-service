package com.blackphoenixproductions.userservice.domain.service;

import com.blackphoenixproductions.userservice.domain.*;
import com.blackphoenixproductions.userservice.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class UserService implements IUserService {

    private final IUserDAO userDAO;
    private final MessagePublisher messagePublisher;
    private final IKeycloakProxy keycloakProxy;
    private final IEmailSender emailSender;

    @Autowired
    public UserService(IUserDAO userDAO, MessagePublisher messagePublisher, @Qualifier("keycloakProxyRest") IKeycloakProxy keycloakProxy, IEmailSender emailSender) {
        this.userDAO = userDAO;
        this.messagePublisher = messagePublisher;
        this.keycloakProxy = keycloakProxy;
        this.emailSender = emailSender;
    }

    @Override
    public User registerUser(String username, String email, String role) {
        User user = userDAO.saveUser(username, email, role);
        emailSender.sendRegistrationEmail(username, email);
        return user;
    }

    @Override
    public User changeUsername(String subject, String email, String newUsername) {
        keycloakProxy.changeUserUsername(subject, newUsername);
        User user = userDAO.changeUsername(email, newUsername);
        messagePublisher.refreshMaterializedViews();
        return user;
    }

    @Override
    public User saveUser(String username, String email, String role) {
        User user = userDAO.saveUser(username, email, role);
        messagePublisher.refreshMaterializedViews();
        return user;
    }
}
