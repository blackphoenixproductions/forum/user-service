package com.blackphoenixproductions.userservice.domain;

import com.blackphoenixproductions.userservice.domain.model.User;

public interface IUserDAO {
    User saveUser(User user);
    User getUserFromEmail(String email);
    Long getTotalUsers();
    User changeUsername(String email, String newUsername);
    User saveUser(String username, String email, String role);
}
