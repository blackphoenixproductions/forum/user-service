package com.blackphoenixproductions.userservice.domain;

public interface MessagePublisher {
    void refreshMaterializedViews();
}
