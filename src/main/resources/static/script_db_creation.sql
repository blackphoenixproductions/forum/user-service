create database user;

CREATE TABLE USERS (
   id bigserial primary key not null,
   username varchar(255) UNIQUE not null,
   email varchar(255) UNIQUE not null,
   role varchar(255) default 'USER'
);